<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PpcrvCount extends Model
{
    protected $fillable = ['candidate_id', 'province_id', 'count'];
}
