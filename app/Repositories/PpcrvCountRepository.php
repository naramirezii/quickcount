<?php

namespace App\Repositories;

use App\PpcrvCount;
use Request;

class PpcrvCountRepository 
{
    /**
     * Get all the active clients
     * 
     * @return App\Models\PpcrvCount Collection;
     */
    public function getAll()
    {
        return PpcrvCount::all();
    }

    public function getAllByPosition($position)
    {
        return PpcrvCount::where('position', $position)->get();
    }

    /**
     * Get all the clients including deactivated
     * 
     * @return App\Models\PpcrvCount Collection;
     */
    public function getAllWithTrashed()
    {
        return PpcrvCount::withTrashed()->get();
    }

    public function lists()
    {
        return PpcrvCount::lists('display_name', 'id');
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\PpcrvCount;
     */
    public function get($id) 
    {
        $ppcrvCount = PpcrvCount::find($id);

        return $ppcrvCount; 
    }

    public function getByCandidateAndProvince($candidateId, $provinceId)
    {
        return PpcrvCount::where('candidate_id', $candidateId)
          ->where('province_id', $provinceId)
          ->first();
    }

    public function create(Array $data) 
    {
        return PpcrvCount::create($data);
    }

    public function update($candidateId, $provinceId, $data)
    {
        $ppcrvCount = $this->getByCandidateAndProvince($candidateId, $provinceId);

        if($ppcrvCount) {
            $ppcrvCount->update($data);
          }
        else {
            $data['candidate_id'] = $candidateId;
            $data['province_id'] = $provinceId;
            $this->create($data);
        }

        return $ppcrvCount;
    }

    public function destroy($id)
    {
        $ppcrvCount = $this->get($id);

        if(is_null($ppcrvCount->deleted_at)) {
            $ppcrvCount->delete();
        }
        else {
            $ppcrvCount->restore();
        }
        
        return $ppcrvCount;
    }

}