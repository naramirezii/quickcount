<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Gate;

class ImportController extends Controller
{
    function __construct()
    {
        ini_set('xdebug.max_nesting_level', 200);
    }

    public function import()
    {
        if(Gate::denies('import')) {
            return redirect('/');
        }
    	
    	return view('input.import')
    		->with('page_title', 'Import from CSV');
    }
}
