@extends('admin_template')

@section('head')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
		{!! Form::file('csv_file') !!}
		<br>
		{!! Form::submit('Import', array('class' => 'btn btn-primary btn-sm btn-flat')) !!}
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
@endsection