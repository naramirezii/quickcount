<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CandidateRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\OfficialCountRepository;

use App\Http\Requests;
use Validator;
use GuzzleHttp\Client;
use Gate;

class OfficialCountController extends Controller
{
    protected $candidate;
    protected $province;
    protected $officialCount;

    function __construct(
        CandidateRepository $candidate,
        ProvinceRepository $province,
        OfficialCountRepository $officialCount)
    {
        ini_set('xdebug.max_nesting_level', 200);

        $this->candidate = $candidate;
        $this->province = $province;
        $this->officialCount = $officialCount;

        $this->middleware('auth');
    }

    public function presidentIndex()
    {
        if(Gate::denies('manual-input')) {
            return redirect('/');
        }

        $provinces = $this->province->getAll();
        $candidates = $this->candidate->getAllByPosition('president');

    	return view('input.manual.president_index')
            ->with('provinces', $provinces)
            ->with('candidates', $candidates)
    		->with('page_title', 'President Quick Count');
    }

    public function vicePresidentIndex()
    {
        if(Gate::denies('manual-input')) {
            return redirect('/');
        }

        $provinces = $this->province->getAll();
        $candidates = $this->candidate->getAllByPosition('vice_president');

        return view('input.manual.vice_president_index')
            ->with('provinces', $provinces)
            ->with('candidates', $candidates)
            ->with('page_title', 'Vice President Quick Count');
    }

    public function editCount($candidateId, $provinceId)
    {
        if(Gate::denies('manual-input')) {
            return redirect('/');
        }

        $officialCount = $this->officialCount->getByCandidateAndProvince($candidateId, $provinceId);

        return view('input.manual.president_edit')
            ->with('officialCount', $officialCount)
            ->with('candidate', $this->candidate->get($candidateId))
            ->with('province', $this->province->get($provinceId))
            ->with('page_title', 'Manual Edit - President Quick Count');
    }

    public function editVicePresidentCount($candidateId, $provinceId)
    {
        if(Gate::denies('manual-input')) {
            return redirect('/');
        }

        $officialCount = $this->officialCount->getByCandidateAndProvince($candidateId, $provinceId);

        return view('input.manual.vice_president_edit')
            ->with('officialCount', $officialCount)
            ->with('candidate', $this->candidate->get($candidateId))
            ->with('province', $this->province->get($provinceId))
            ->with('page_title', 'Manual Edit - Vice President Quick Count');
    }

    public function updateCount(Request $request, $candidateId, $provinceId)
    {
        if(Gate::denies('manual-input')) {
            return redirect('/');
        }

        $this->validate($request, [
            'count' => 'numeric',
        ]);

        $officialCount = $this->officialCount->update($candidateId, $provinceId, $request->all());

        $candidate = $this->candidate->get($candidateId);
        $province = $this->province->get($provinceId);

        $this->officialCount->updateCartoDb($candidate->code, $province->code, $request->count);

        $request->session()->flash('success_message', 'Successfully updated quick count for, <strong>'.$candidate->first_name.' '.$candidate->last_name.' - '.$province->name.'</strong>.');

        return redirect()->route('manual');
    }

    public function updateVicePresidentCount(Request $request, $candidateId, $provinceId)
    {
        if(Gate::denies('manual-input')) {
            return redirect('/');
        }

        $this->validate($request, [
            'count' => 'numeric',
        ]);
        
        $officialCount = $this->officialCount->update($candidateId, $provinceId, $request->all());

        $candidate = $this->candidate->get($candidateId);
        $province = $this->province->get($provinceId);

        $this->officialCount->updateCartoDb($candidate->code, $province->code, $request->count);

        $request->session()->flash('success_message', 'Successfully updated quick count for, <strong>'.$candidate->first_name.' '.$candidate->last_name.' - '.$province->name.'</strong>.');

        return redirect()->route('manual.vice-president');
    }

}
