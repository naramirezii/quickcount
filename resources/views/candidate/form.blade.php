<div class="row">
  <div class="col-xs-12">
    <div class="box">
    	<div class="box-header with-border">
        <!-- <h3 class="box-title">Enroller Profile</h3> -->
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="form-group  {{ $errors->has('photo') ? 'has-error' : '' }}">
          <label>Photo</label>
          <br>
          @if($mode === 'edit')
            @if(isset($candidate) && $candidate->photo)
              <img src="{{ url($candidate->photo) }}" alt="User Image" height="100px" />
            @else
              <img src="{{ asset('images/default-user.png') }}" alt="User Image" />
            @endif
          @endif
          <input type="file" name="photo">
        </div>
        <div class="form-group  {{ $errors->has('first_name') ? 'has-error' : '' }}">
          <label>First Name</label>
          {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group  {{ $errors->has('last_name') ? 'has-error' : '' }}">
          <label>Last Name</label>
          {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group  {{ $errors->has('middle_name') ? 'has-error' : '' }}">
          <label>Middle Name</label>
          {!! Form::text('middle_name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group  {{ $errors->has('position') ? 'has-error' : '' }}">
          <label>Position</label>
          {!! Form::select('position', $positions, null, ['placeholder' => '- Select Position -', 'class' => 'form-control', 'style' => 'width:100%']) !!}
        </div>
        <div class="form-group  {{ $errors->has('code') ? 'has-error' : '' }}">
          <label>Code</label>
          {!! Form::text('code', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group  {{ $errors->has('color') ? 'has-error' : '' }}">
          <label>Color</label>
          {!! Form::text('color', null, ['class' => 'form-control']) !!}
        </div>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->