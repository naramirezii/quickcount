<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CandidateRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\RegionRepository;
use App\Repositories\OfficialCountRepository;

use App\Http\Requests;
use Gate;

class ReportController extends Controller
{
    protected $candidate;
    protected $province;
    protected $region;
    protected $officialCount;

    function __construct(
        CandidateRepository $candidate,
        ProvinceRepository $province,
        RegionRepository $region,
        OfficialCountRepository $officialCount)
    {
        $this->candidate = $candidate;
        $this->province = $province;
        $this->region = $region;
        $this->officialCount = $officialCount;

        $this->middleware('auth');
    }

    public function discrepanciesPresident()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }

        $candidates = $this->candidate->getAllByPosition('president');
        $provinces = $this->province->getAll();

    	return view('report.discrepancies_president')
            ->with('candidates', $candidates)
            ->with('provinces', $provinces)
            ->with('page_title', 'Discrepancies Report (President)');
    }

    public function discrepanciesVicePresident()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }

        $candidates = $this->candidate->getAllByPosition('vice_president');
        $provinces = $this->province->getAll();

        return view('report.discrepancies_vice_president')
            ->with('candidates', $candidates)
            ->with('provinces', $provinces)
            ->with('page_title', 'Discrepancies Report (Vice President)');
    }

    public function registeredVsActualPresident()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }

        $provinces = $this->province->getAll();

    	return view('report.registered_vs_actual_president')
            ->with('provinces', $provinces)
            ->with('page_title', 'Registered vs Actual (President)');
    }

    public function registeredVsActualVicePresident()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }
        
        $provinces = $this->province->getAll();

        return view('report.registered_vs_actual_vice_president')
            ->with('provinces', $provinces)
            ->with('page_title', 'Registered vs Actual (Vice President)');
    }

    public function registeredVsCocPresident()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }
        
        $provinces = $this->province->getAll();

        return view('report.registered_vs_coc_president')
            ->with('provinces', $provinces)
            ->with('page_title', 'Registered vs Actual - COC (President)');
    }

    public function registeredVsCocVicePresident()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }
        
        $provinces = $this->province->getAll();

        return view('report.registered_vs_coc_vice_president')
            ->with('provinces', $provinces)
            ->with('page_title', 'Registered vs Actual - COC (Vice President)');
    }

    public function votersTurnoutRegional()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }

        $regions = $this->region->getAll();
        $overallVotersTurnout = $this->officialCount->getOverallVotersTurnout();

        return view('report.voters_turnout_regional')
            ->with('regions', $regions)
            ->with('overallVotersTurnout', $overallVotersTurnout)
            ->with('page_title', 'Voters Turnout - Regional');
    }

    public function votersTurnoutProvincial()
    {
        if(Gate::denies('view-reports')) {
            return redirect('/');
        }

        $provinces = $this->province->getAll();
        $overallVotersTurnout = $this->officialCount->getOverallVotersTurnout();

        return view('report.voters_turnout_provincial')
            ->with('provinces', $provinces)
            ->with('overallVotersTurnout', $overallVotersTurnout)
            ->with('page_title', 'Voters Turnout - Provincial');
    }
}
