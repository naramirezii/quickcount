<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Province extends Model
{
    public function region()
    {
    	return $this->belongsTo(Region::class);
    }

    public function getCandidateQuickCount($candidateId, $provinceId)
    {
    	$quickCount = OfficialCount::where('candidate_id', $candidateId)
    		->where('province_id', $provinceId)
            ->orderBy('id', 'desc')
    		->first();

    	if($quickCount)
    		return $quickCount->count;
    	else
    		return null;
    }

    public function getCandidatePpcrvCount($candidateId, $provinceId)
    {
        $ppcrvCount = PpcrvCount::where('candidate_id', $candidateId)
            ->where('province_id', $provinceId)
            ->orderBy('id', 'desc')
            ->first();

        if($ppcrvCount)
            return $ppcrvCount->count;
        else
            return null;
    }

    public function getCandidateCocVsQuickCount($candidateId, $provinceId)
    {
        return $this->getCandidatePpcrvCount($candidateId, $provinceId) - $this->getCandidateQuickCount($candidateId, $provinceId);
    }

    public function getActualVotingCount($position, $provinceId)
    {
        $count = 0;
        $candidates = Candidate::where('position', $position)->get();

        foreach($candidates as $candidate) {
            $count += $this->getCandidateQuickCount($candidate->id, $provinceId);
        }

        return $count;
    }

    public function getRegisteredVsActualCount($position, $provinceId)
    {
        return $this->registered_voters - $this->getActualVotingCount($position, $provinceId);
    }

    public function getRegisteredVsCocCount($position, $provinceId)
    {
        return $this->registered_voters - $this->getCocVotersTurnout($position, $provinceId);
    }

    public function monitoring()
    {
        return $this->hasOne(Monitoring::class);
    }

    public function getCandidatePercentage($candidateId, $provinceId)
    {
        $candidate = Candidate::find($candidateId);

        if($this->getVotersTurnout($candidate->position, $provinceId)) {
            $result = $this->getCandidateQuickCount($candidateId, $provinceId) / $this->getVotersTurnout($candidate->position, $provinceId) * 100;
            return number_format($result, 2, '.', ',');
        }
    }

    public function getCandidateCocPercentage($candidateId, $provinceId)
    {
        $candidate = Candidate::find($candidateId);

        if($this->getCocVotersTurnout($candidate->position, $provinceId)) {
            $result = $this->getCandidatePpcrvCount($candidateId, $provinceId) / $this->getCocVotersTurnout($candidate->position, $provinceId) * 100;
            return number_format($result, 2, '.', ',');
        }
    }

    public function getVotersTurnout($position, $provinceId)
    {
        $count = 0;
        $candidates = Candidate::where('position', $position)->get();

        foreach($candidates as $candidate) {
            $count += $this->getCandidateQuickCount($candidate->id, $provinceId);
        }

        return $count;
    }

    public function getVotersTurnoutPercentage()
    {
        if($this->getVotersTurnout('president', $this->id))
            return $this->getVotersTurnout('president', $this->id) / $this->registered_voters * 100;
        else
            return null;
    }

    public function getCocVotersTurnout($position, $provinceId)
    {
        $count = 0;
        $candidates = Candidate::where('position', $position)->get();

        foreach($candidates as $candidate) {
            $count += $this->getCandidatePpcrvCount($candidate->id, $provinceId);
        }

        return $count;
    }

    public function getCocVotersTurnoutPercentage()
    {
        if($this->getCocVotersTurnout('president', $this->id))
            return $this->getCocVotersTurnout('president', $this->id) / $this->registered_voters * 100;
        else
            return null;
    }
}
