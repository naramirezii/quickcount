@extends('admin_template')

@section('head')
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@endsection

@section('content')
	{!! Form::model($candidate, ['method' => 'POST', 'route' => ['admin.candidate.store'], 'files' => true]) !!}
	    @include('candidate.form', ['mode' => 'create'])
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
	    			<button type="submit" class="btn btn-primary btn-flat">Create Candidate</button>
		            <a href="{{ route('admin.candidate.index') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    </div>
		</div>
	{!! Form::close() !!}	    
@endsection

@section('script')
	<script src="{{ asset("admin_theme/plugins/jQueryUI/jquery-ui.min.js") }}" type="text/javascript"></script>

	<script>
	  $(function() {
	    $( "#birthdate" ).datepicker({
	      changeMonth: true,
	      changeYear: true,
	      dateFormat: 'yy-mm-dd'
	    });
	  });
	</script>
@endsection