@extends('admin_template')

@section('head')
	<!-- DATA TABLES -->
    <link href="{{ asset('admin_theme/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="box">
	      	<div class="box-header">
	      	</div><!-- /.box-header -->
	      	<div class="box-body">
		      	<table id="data_table" class="table table-bordered table-striped">
		      		<thead>
		      			<tr>
		      				<th>Province</th>
		      				<th>Zip Code</th>
		      				<th>Region</th>
		      				<th></th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      		@foreach($provinces as $province)
		      			<tr>
		      				<td>{{ $province->name }}</td>
		      				<td>{{ $province->zip_code }}</td>
		      				<td>{{ $province->region->name }}</td>
		      				<td align="center"><a href="{{ route('ppcrv-coc.edit', $province->id) }}"><i class="fa fa-pencil"></i></a></td>
		      			</tr>
		      		@endforeach
		      		</tbody>
		      	</table>
		    </div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
	<!-- DATA TABLES SCRIPT -->
	<script src="{{ asset("admin_theme/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
	<script src="{{ asset("admin_theme/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>

	<script type="text/javascript">
		$(function () {
			var table = $('#data_table').DataTable({
				"paging": false,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"autoWidth": true,
				"scrollX": true
			});
		});		
	</script>
@endsection