<?php

namespace App\Repositories;

use App\Candidate;
use Request, Input, Image, DB;

class CandidateRepository 
{
    /**
     * Get all the active clients
     * 
     * @return App\Models\Candidate Collection;
     */
    public function getAll()
    {
        return Candidate::all();
    }

    public function getAllByPosition($position)
    {
        return Candidate::where('position', $position)->get();
    }

    public function getAllPresidentsAndVps()
    {
        return Candidate::where('position', 'president')
            ->orWhere('position', 'vice_president')->get();
    }

    /**
     * Get all the clients including deactivated
     * 
     * @return App\Models\Candidate Collection;
     */
    public function getAllWithTrashed()
    {
        return Candidate::withTrashed()->get();
    }

    public function lists()
    {
        return Candidate::lists('display_name', 'id');
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\Candidate;
     */
    public function get($id) 
    {
        $candidate = Candidate::withTrashed()->find($id);

        return $candidate; 
    }

    public function create(Array $data) 
    {
        return Candidate::create($data);
    }

    public function update($id, $data)
    {
        $candidate = $this->get($id);

        // Photo
        $file = null;
        isset($data['photo']) ? $file = $data['photo'] : null;
        if($file)
        {
            if ($file->isValid())
            {
              $destinationPath = 'uploads/candidate_photos/';
              $extension = $file->getClientOriginalExtension();
              $fileName = rand(11111,99999).'.'.$extension;
              $file->move($destinationPath, $fileName);

              $data['photo'] = '/'.$destinationPath.$fileName;
            }
        }

        $candidate->update($data);

        return $candidate;
    }

    public function destroy($id)
    {
        $candidate = $this->get($id);

        if(is_null($candidate->deleted_at)) {
            $candidate->delete();
        }
        else {
            $candidate->restore();
        }
        
        return $candidate;
    }


}