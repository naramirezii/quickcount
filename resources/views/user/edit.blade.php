@extends('admin_template')

@section('content')
	{!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.user.update', $user->id], 'files' => true]) !!}
	    @include('user.form', ['mode' => 'edit'])
	    {!! Form::submit('', ['id' => 'submit-form', 'class' => 'hidden']) !!}
	{!! Form::close() !!}
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
		    		<label for="submit-form" class="btn btn-primary btn-flat">Update User</label>
		            <a href="{{ route('admin.user.index') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    	<div class="pull-right">
		    		{!! Form::open(['method' => 'DELETE', 'route' => ['admin.user.destroy', $user->id]]) !!}
						@if(is_null($user->deleted_at))
							<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return confirm('Are you sure you want to delete user?')"><strong>Delete User</strong></button>
						@endif
						<a href="{{ route('admin.user.change-password', $user->id) }}" class="btn btn-default btn-sm btn-flat"><strong>Change Password</strong></a>
					{!! Form::close() !!}
		    	</div>
		    </div>
		</div>
	{!! Form::close() !!}
@endsection