<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CandidateRepository;
use App\Repositories\OfficialCountRepository;

use App\Http\Requests;
use DB;
use Gate;

class ConfigController extends Controller
{
    protected $candidate;
    protected $officialCount;

    function __construct(
        CandidateRepository $candidate,
        OfficialCountRepository $officialCount)
    {
        ini_set('xdebug.max_nesting_level', 200);

        $this->candidate = $candidate;
        $this->officialCount = $officialCount;

        $this->middleware('auth');
    }

    public function resetDatabase()
    {
        if(Gate::denies('reset-database')) {
            return redirect('/');
        }

    	return view('config.reset_database')
    		->with('page_title', 'Reset Database');
    }

    public function saveResetDatabase(Request $request)
    {
        if(Gate::denies('reset-database')) {
            return redirect('/');
        }
        
        DB::table('official_counts')->delete();
        DB::table('ppcrv_counts')->delete();
        DB::table('monitorings')->delete();

        $candidates = $this->candidate->getAllPresidentsAndVps();

        // Reset CartoDB count
        foreach ($candidates as $candidate) {
            $this->officialCount->resetCandidateCount($candidate->code);
        }

        $request->session()->flash('success_message', 'Successfully reset database.');

        return redirect()->route('config.reset-database');
    }
}
