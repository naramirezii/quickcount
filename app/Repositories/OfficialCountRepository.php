<?php

namespace App\Repositories;

use App\OfficialCount;
use App\Province;
use App\Candidate;
use Request;
use GuzzleHttp\Client;
use DB;

class OfficialCountRepository 
{
    /**
     * Get all the active clients
     * 
     * @return App\Models\OfficialCount Collection;
     */
    public function getAll()
    {
        return OfficialCount::all();
    }

    public function getAllByPosition($position)
    {
        return OfficialCount::where('position', $position)->get();
    }

    /**
     * Get all the clients including deactivated
     * 
     * @return App\Models\OfficialCount Collection;
     */
    public function getAllWithTrashed()
    {
        return OfficialCount::withTrashed()->get();
    }

    public function lists()
    {
        return OfficialCount::lists('display_name', 'id');
    }

    public function getRemittedProvinces()
    {
        return OfficialCount::select('province_id')
            ->groupBy('province_id')
            ->get();
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\OfficialCount;
     */
    public function get($id) 
    {
        $officialCount = OfficialCount::find($id);

        return $officialCount; 
    }

    public function getByCandidateAndProvince($candidateId, $provinceId)
    {
        return OfficialCount::where('candidate_id', $candidateId)
          ->where('province_id', $provinceId)
          ->orderBy('id', 'desc')
          ->first();
    }

    public function create(Array $data) 
    {
        return OfficialCount::create($data);
    }

    public function update($candidateId, $provinceId, $data)
    {
        $officialCount = $this->getByCandidateAndProvince($candidateId, $provinceId);

        if($officialCount) {
            $officialCount->update($data);
          }
        else {
            $data['candidate_id'] = $candidateId;
            $data['province_id'] = $provinceId;
            $this->create($data);
        }

        return $officialCount;
    }

    public function updateCartoDb($candidate, $province, $count)
    {
        $client = new Client();

        $response = $client->get("https://quickcount2016.cartodb.com:443/api/v2/sql?q=UPDATE quickcount2016 SET ".$candidate." = ".$count." where area_code = ".$province."&api_key=d1946b1d250e85622ad8927e54c6f5f97606c7aa");
    }

    public function resetCandidateCount($candidate)
    {
        $client = new Client();
        
        $response = $client->get("https://quickcount2016.cartodb.com:443/api/v2/sql?q=UPDATE quickcount2016 SET ".$candidate." = 0 &api_key=d1946b1d250e85622ad8927e54c6f5f97606c7aa");
    }

    public function destroy($id)
    {
        $officialCount = $this->get($id);

        if(is_null($officialCount->deleted_at)) {
            $officialCount->delete();
        }
        else {
            $officialCount->restore();
        }
        
        return $officialCount;
    }

    public function getOverallVotersTurnout()
    {
        $totalRegisteredVoters = Province::select(DB::raw('SUM(registered_voters) as registered_voters'))
            ->first()->registered_voters;

        $totalQuickCount = $this->getOverallVoteCount('president');

        if($totalQuickCount)
            return $totalQuickCount / $totalRegisteredVoters;
        else
            return null;
    }

    public function getOverallVoteCount($position)
    {
        $candidates = Candidate::select('candidates.*')
            ->where('position', $position)
            ->join('official_counts', 'official_counts.candidate_id', '=', 'candidates.id')
            ->groupBy('candidate_id')
            ->get();

        $count = 0;

        foreach ($candidates as $candidate) {
            $provinces = Province::all();

            foreach($provinces as $province) {
                $quickCount = OfficialCount::where('candidate_id', $candidate->id)
                    ->where('province_id', $province->id)
                    ->orderBy('id', 'desc')
                    ->first();

                if($quickCount) $count += $quickCount->count;
            }  
        }

        return $count;
    }

    public function getOverallRankingByPosition($position)
    {
        $candidates = Candidate::select('candidates.*')
            ->where('position', $position)
            ->join('official_counts', 'official_counts.candidate_id', '=', 'candidates.id')
            ->groupBy('candidate_id')
            ->get();

        foreach ($candidates as $candidate) {
            $count = 0;
            $provinces = Province::all();

            foreach($provinces as $province) {
                $quickCount = OfficialCount::where('candidate_id', $candidate->id)
                    ->where('province_id', $province->id)
                    ->orderBy('id', 'desc')
                    ->first();

                if($quickCount) $count += $quickCount->count;
            }
            
            $candidate->overall_count = $count;   
        }
        
        return $candidates->sortByDesc('overall_count');
    }

    public function getRemittanceRate()
    {
        return sizeof($this->getRemittedProvinces())/Province::count() * 100;
    }
}