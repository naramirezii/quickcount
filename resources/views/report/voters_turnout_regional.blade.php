@extends('admin_template')

@section('head')
	<!-- DATA TABLES -->
    <link href="{{ asset('admin_theme/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 text-center">
		<a href="{{ route('report.voters-turnout-regional') }}" class="btn btn-primary btn-flat btn-sm">Regional</a>
		<a href="{{ route('report.voters-turnout-provincial') }}" class="btn btn-default btn-flat btn-sm">Provincial</a>
		<br><br>
	</div>
</div>
<div class="row">

	<div class="col-xs-12">
		<h4 class="box-title">Overall Voters Turnout: <strong>{{ number_format($overallVotersTurnout, 4, '.', ',') }}%</strong></h4>
		<div class="box">
	      	<div class="box-header">
	      	</div><!-- /.box-header -->
	      	<div class="box-body">
		      	<table id="data_table" class="table table-bordered table-striped">
		      		<thead>
		      			<tr>
		      				<th>Region</th>
		      				<th>Registered Voters</th>
		      				<th>Actual</th>
		      				<th>Voters Turnout</th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      		@foreach($regions as $region)
		      			<tr>
		      				<td width="40%">{{ $region->name }}</td>
		      				<td align="right">{{ number_format($region->getRegisteredVoters($region->id)) }}</td>
		      				<td align="right">
		      					@if($region->getVotersTurnout('president', $region->id))
		      						{{ number_format($region->getVotersTurnout('president', $region->id)) }}
		      					@endif
		      				</td>
		      				<td align="right">
		      					@if($region->getVotersTurnoutPercentage($region->id))
		      						{{ number_format($region->getVotersTurnoutPercentage($region->id), 2, '.', ',') }}%
		      					@endif
		      				</td>
		      			</tr>
		      		@endforeach
		      		</tbody>
		      	</table>
		    </div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
	<!-- DATA TABLES SCRIPT -->
	<script src="{{ asset("admin_theme/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
	<script src="{{ asset("admin_theme/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(function () {
			var table = $('#data_table').DataTable({
				"paging": false,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"autoWidth": true,
				"scrollX": true,
				dom: 'Bfrtip',
				buttons: [
		            'print'
		        ]
			});
		});		
	</script>
@endsection