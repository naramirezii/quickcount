@extends('admin_template')

@section('head')
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 text-center">
		<a href="{{ route('ranking.count-received-president') }}" class="btn btn-primary btn-flat btn-sm">Deployment Area (President)</a>
		<a href="{{ route('ranking.count-received-vice-president') }}" class="btn btn-default btn-flat btn-sm">Deployment Area (VP)</a>
		<a href="{{ route('ranking.per-area-president') }}" class="btn btn-default btn-flat btn-sm">President by Deployment Area</a>
		<a href="{{ route('ranking.per-area-vice-president') }}" class="btn btn-default btn-flat btn-sm">VP by Deployment Area</a>
		<br><br>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<h4>Remittance Rate: <strong>{{ $remittedProvincesCount }}/{{ $totalProvinces }}</strong> (<strong>{{ number_format($remittanceRate) }}%</strong>)</h4>
		<div class="box">
	      	<div class="box-header">
	      	</div><!-- /.box-header -->
	      	<div class="box-body">
	      		<iframe width="100%" height="520" frameborder="0" src="https://quickcount2016.cartodb.com/viz/1e332842-04b2-11e6-a73c-0e787de82d45/embed_map" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
		    </div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
@endsection