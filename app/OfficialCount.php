<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfficialCount extends Model
{
    protected $fillable = ['candidate_id', 'province_id', 'count'];
}
