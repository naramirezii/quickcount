@extends('admin_template')

@section('content')
	<form action="{{ route('admin.user.update-password', $user->id) }}" method="POST" enctype="multipart/form-data">
		{!! csrf_field() !!}

	    <div class="row">
		    <div class="col-xs-12">
		      <div class="box">
		        <div class="box-body">
		        	<h4>{{ $user->name }}</h4>

	                <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
	                  <label>New Password</label>
	                  <input type="password" name="password" class="form-control" />
	                </div>

	                <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
	                  <label>Confirm Password</label>
	                  <input type="password" name="password_confirmation" class="form-control" />
	                </div>
		        </div><!-- /.box-body -->
		      </div><!-- /.box -->
		    </div><!-- /.col -->
		</div><!-- /.row -->

		<div class="row">
			<div class="col-xs-12">
				<button type="submit" class="btn btn-primary btn-flat">Change User Password</button>
				<a href="{{ route('admin.user.edit', $user->id) }}" class="btn"><u>Cancel</u></a>
			</div>
		</div>

	</form>	  
@endsection