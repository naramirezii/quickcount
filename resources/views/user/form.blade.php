<div class="row">
  <div class="col-xs-12">
    <div class="box">
    	<div class="box-header with-border">
        <!-- <h3 class="box-title">Enroller Profile</h3> -->
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
          <label>Name</label>
          {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group  {{ $errors->has('role') ? 'has-error' : '' }}">
          <label>Role</label>
          {!! Form::select('role', $roles, null, ['placeholder' => '- Select Role -', 'class' => 'form-control', 'style' => 'width:100%']) !!}
        </div>
        <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
          <label>Username</label>
          {!! Form::text('email', null, ['class' => 'form-control']) !!}
        </div>

        @if($mode === 'create')
          <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
            <label>Password</label>
            {!! Form::password('password', ['class' => 'form-control']) !!}
          </div>

          <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
            <label>Confirm Password</label>
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
          </div>
        @endif
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!-- /.col -->
</div><!-- /.row -->