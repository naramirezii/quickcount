<?php

namespace App\Repositories;

use App\Monitoring;
use Request, Input, Image, DB;

class MonitoringRepository 
{
    /**
     * Get all the active clients
     * 
     * @return App\Models\Monitoring Collection;
     */
    public function getAll()
    {
        return Monitoring::all();
    }

    /**
     * Get all the clients including deactivated
     * 
     * @return App\Models\Monitoring Collection;
     */
    public function getAllWithTrashed()
    {
        return Monitoring::withTrashed()->get();
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\Monitoring;
     */
    public function get($id) 
    {
        $monitoring = Monitoring::withTrashed()->find($id);

        return $monitoring; 
    }

    public function getByProvince($provinceId)
    {
        return Monitoring::where('province_id', $provinceId)->first();
    }

    public function create(Array $data) 
    {
        return Monitoring::create($data);
    }

    public function update($provinceId, $data)
    {
        $monitoring = $this->getByProvince($provinceId);
        
        if(!isset($data['text'])) $data['text'] = 0;
        if(!isset($data['viber'])) $data['viber'] = 0;
        if(!isset($data['call'])) $data['call'] = 0;

        if($monitoring) {
            $monitoring->update($data);
        }
        else {
            $data['province_id'] = $provinceId;
            $this->create($data);
        }

        return $monitoring;
    }

    public function batchTaggingOfText($remittedProvinces)
    {
        foreach($remittedProvinces as $province) {
            $monitoring = $this->getByProvince($province->province_id);

            $data['text'] = 1;

            if($monitoring) {
                $monitoring->update($data);
            }
            else {
                $data['province_id'] = $province->province_id;
                $this->create($data);
            }
        }
    }

    public function destroy($id)
    {
        $monitoring = $this->get($id);

        if(is_null($monitoring->deleted_at)) {
            $monitoring->delete();
        }
        else {
            $monitoring->restore();
        }
        
        return $monitoring;
    }
}