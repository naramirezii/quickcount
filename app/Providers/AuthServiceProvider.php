<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('view-reports', function ($user) {
            return true;
        });

        // Votes Input
        $gate->define('import', function ($user) {
            return $user->role === 'admin';
        });
        $gate->define('manual-input', function ($user) {
            return $user->role === 'admin'
                || $user->role === 'checker';
        });
        $gate->define('coc-input', function ($user) {
            return $user->role === 'admin'
                || $user->role === 'encoder'
                || $user->role === 'checker';
        });
        $gate->define('monitoring', function ($user) {
            return $user->role === 'admin'
                || $user->role === 'encoder'
                || $user->role === 'checker';
        });

        // Admin
        $gate->define('manage-admin', function ($user) {
            return $user->role === 'admin';
        });
        $gate->define('manage-candidates', function ($user) {
            return $user->role === 'admin';
        });
        $gate->define('manage-users', function ($user) {
            return $user->role === 'admin';
        });
        $gate->define('reset-database', function ($user) {
            return $user->role === 'admin';
        });
    }
}
