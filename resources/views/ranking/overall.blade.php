@extends('admin_template')

@section('head')
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 text-center">
		<a href="{{ route('ranking.overall') }}" class="btn btn-primary btn-flat btn-sm">President</a>
		<a href="{{ route('ranking.overall-vice-president') }}" class="btn btn-default btn-flat btn-sm">Vice President</a>
		<br><br>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
	      	<div class="box-header">
	      		<h3 class="box-title">President</h3>
	      	</div><!-- /.box-header -->
	      	<div class="box-body">

		      	<div id="chart_div"></div>

		    </div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<script type="text/javascript">
		google.charts.load('current', {packages: ['corechart', 'bar']});
		google.charts.setOnLoadCallback(drawStacked);

		function drawStacked() {
			var dataTable = new google.visualization.DataTable();
			dataTable.addColumn('string', 'Candidate');
			dataTable.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
			dataTable.addColumn('number', 'Votes');
			dataTable.addColumn({'type': 'string', 'role': 'style'});
			dataTable.addColumn({'type': 'string', 'role': 'annotation'});

			dataTable.addRows([
				@foreach($candidates as $candidate)
					['{{ $candidate->name }}',
						createCustomHTMLContent('{{ $candidate->photo }}', '{{ number_format($candidate->overall_count) }}', {{ number_format(($candidate->overall_count / $overallVoteCount), 2, '.', ',') * 100 }}),
						{{ $candidate->overall_count }},
						'{{ $candidate->color }}',
						'{{ number_format(($candidate->overall_count / $overallVoteCount), 2, '.', ',') * 100 }}% ({{ number_format($candidate->overall_count) }})'
					],
				@endforeach
			]);

			var options = {
				// This line makes the entire category's tooltip active.
				focusTarget: 'category',
				// Use an HTML tooltip.
				tooltip: { isHtml: true },
				legend: { position: "none" },
				height: 500,
			};

			// Create and draw the visualization.
			new google.visualization.ColumnChart(document.getElementById('chart_div')).draw(dataTable, options);
		}


		function createCustomHTMLContent(photo, votes, percentage) {
			return '<div style="padding:5px 20px 5px 20px;">' +
				'<img src="' + photo + '" style="width:75px;height:75px"><br/>' +
				'<table>' +

				'<tr>' +
				'<td align="center"><h4>' + votes + '</h4></td>' +
				'</tr>' +

				'<tr>' +
				'<td align="center"><h4>' + percentage + '%</h4></td>' +
				'</tr>'

				+ '</table>' + '</div>';
		}
	</script>
@endsection