@extends('admin_template')

@section('head')
	<!-- DATA TABLES -->
    <link href="{{ asset('admin_theme/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 text-center">
		<a href="{{ route('report.discrepancies-president') }}" class="btn btn-primary btn-flat btn-sm">President</a>
		<a href="{{ route('report.discrepancies-vice-president') }}" class="btn btn-default btn-flat btn-sm">Vice President</a>
		<br><br>
	</div>
</div>

<div class="panel-group" id="accordion">
	@foreach($candidates as $candidate)
	    <div class="panel panel-default">
	        <div class="panel-heading">
	            <h4 class="panel-title">
	                <a data-toggle="collapse" data-parent="#accordion" href="#table_area_{{ $candidate->code }}">
	                   {{ $candidate->name }}
	                </a>
	            </h4>
	        </div>

	        <div id="table_area_{{ $candidate->code }}" class="panel-collapse collapse">
	            <div class="panel-body">
	                 <table id="{{ $candidate->code }}" class="table table-bordered table-striped">
	                    <thead>
	                        <tr>
			      				<th>Province</th>
			      				<th>Region</th>
			      				<th>COC</th>
			      				<th>Quick Count</th>
			      				<th>Discrepancy</th>
			      			</tr>
	                    </thead>
	             
	                    <tbody>
				      		@foreach($provinces as $province)
				      			<tr>
				      				<td>{{ $province->name }} ({{ $province->code }})</td>
				      				<td>{{ $province->region->name }}</td>
				      				<td align="right">
				      					@if($province->getCandidatePpcrvCount($candidate->id, $province->id))
				      						{{ number_format($province->getCandidatePpcrvCount($candidate->id, $province->id)) }}
				      					@endif
				      				</td>
				      				<td align="right">
				      					@if($province->getCandidateQuickCount($candidate->id, $province->id))
				      						{{ number_format($province->getCandidateQuickCount($candidate->id, $province->id)) }}
				      					@endif
				      				</td>
				      				<td align="right" 
					      				@if($province->getCandidateCocVsQuickCount($candidate->id, $province->id) != 0)
					      					class="danger"
					      				@endif
					      				>
					      				@if($province->getCandidateCocVsQuickCount($candidate->id, $province->id))
					      					{{ number_format($province->getCandidateCocVsQuickCount($candidate->id, $province->id)) }}
					      				@endif
				      				</td>
				      			</tr>
				      		@endforeach
				      	</tbody>
	                </table>        
	            </div>
	        </div>
	    </div>
	@endforeach
</div>

@endsection

@section('script')
	<!-- DATA TABLES SCRIPT -->
	<script src="{{ asset("admin_theme/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
	<script src="{{ asset("admin_theme/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			@foreach($candidates as $candidate)
			    var table = $("#{{ $candidate->code }}").dataTable({
					"paging": false,
					"lengthChange": false,
					"searching": true,
					"ordering": true,
					"info": false,
					"autoWidth": true,
					"scrollX": true,
					dom: 'Bfrtip',
					buttons: [
			            'print'
			        ]
			    });
		   	@endforeach

		    /* Set up an event listner to fire after the container has been made visible */
		    $(".panel-collapse").on('shown.bs.collapse', function(){
		        var tbl = $("table", $(this));
		        $(tbl).DataTable().columns.adjust();
		    });
		});
	</script>
@endsection