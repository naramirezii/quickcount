@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
    	<p><strong>Check form for the following errors:</strong></p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success_message'))
<div class="alert alert-success alert-dismissable">
	<p>{!! session('success_message') !!}</p>
</div>
@endif