@extends('admin_template')

@section('head')
@endsection

@section('content')

<div class="row">
	<div class="col-xs-12">
		<div class="alert alert-info">
			<i class="icon fa fa-info"></i>The following tables will be truncated: official_counts, ppcrv_counts, monitorings.
		</div>
		{!! Form::open(['route' => ['config.save-reset-database'], 'onsubmit' => "return confirm('Do you really want to reset the database?');"]) !!}
		{!! Form::submit('Reset Database', array('class' => 'btn btn-danger btn-sm btn-flat')) !!}
		{!! Form::close() !!}
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
@endsection