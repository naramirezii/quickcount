<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Gate;

class UserController extends Controller
{
    function __construct()
    {
        ini_set('xdebug.max_nesting_level', 200);

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }

        $users = User::all();

        return view('user.index')
            ->with('users', $users)
            ->with('page_title', 'List of Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }

        $roles = $this->getRoles();

        return view('user.create')
            ->with('user', null)
            ->with('roles', $roles)
            ->with('page_title', 'Create User');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }

        $this->validate($request, [
            'name' => 'required',
            'role' => 'required',
            'email' => 'required|unique:users|max:150',
            'password' => 'required|confirmed',
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $request->session()->flash('success_message', 'Successfully added user, <strong>'.$request->name.'</strong>.');

        return redirect()->route('admin.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }

        $user = User::find($id);
        $roles = $this->getRoles();

        return view('user.edit')
            ->with('user', $user)
            ->with('roles', $roles)
            ->with('page_title', 'Edit User');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }
        
        $this->validate($request, [
            'name' => 'required',
            'role' => 'required',
        ]);
        
        $user = User::find($id);
        $user->name = $request->name;
        $user->role = $request->role;
        $user->email = $request->email;
        $user->save();

        $request->session()->flash('success_message', 'Successfully updated user, <strong>'.$request->name.'</strong>.');

        return redirect()->route('admin.user.edit', $id);
    }

    public function changePassword($id)
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }

        $user = User::find($id);

        return view('user/change_password')
            ->with('user', $user)
            ->with('page_title', 'Change Password');
    }

    public function updatePassword(Request $request, $id)
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }
        
        $this->validate($request, [
            'password' => 'required|confirmed'
        ]);

        $user = User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();

        $request->session()->flash('success_message', 'Successfully updated password of user, <strong>'.$user->name.'</strong>.');

        return redirect()->route('admin.user.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(Gate::denies('manage-users')) {
            return redirect('/');
        }

        $user = User::find($id);
        $user->delete();
        
        $request->session()->flash('success_message', 'Successfully deleted user, <strong>'.$user->name.'</strong>.');
        
        return redirect()->route('admin.user.index');
    }

    private function getRoles()
    {
        return array('admin' => 'Admin',
            'encoder' => 'Encoder',
            'checker' => 'Checker');
    }
}
