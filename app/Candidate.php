<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Candidate extends Model
{
    use SoftDeletes;

    protected $fillable = ['first_name', 'last_name', 'middle_name', 'display_name',
    	'code', 'position', 'photo', 'remarks', 'color', 'last_updated_by'];

    protected $dates = ['deleted_at'];

    public function getOverallQuickCount()
    {
        $count = 0;
        $provinces = Province::all();

        foreach($provinces as $province) {
            $quickCount = OfficialCount::where('candidate_id', $this->id)
                ->where('province_id', $province->id)
                ->orderBy('id', 'desc')
                ->first();

            if($quickCount) $count += $quickCount;
        }
       
        return $count;
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }
}
