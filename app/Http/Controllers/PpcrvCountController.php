<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PpcrvCountRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\CandidateRepository;

use App\Http\Requests;
use Gate;

class PpcrvCountController extends Controller
{
    protected $province;
    protected $candidate;
    protected $ppcrvCount;

    function __construct(
        ProvinceRepository $province,
        CandidateRepository $candidate,
        PpcrvCountRepository $ppcrvCount)
    {
        ini_set('xdebug.max_nesting_level', 200);

        $this->province = $province;
        $this->candidate = $candidate;
        $this->ppcrvCount = $ppcrvCount;

        $this->middleware('auth');
    }

    public function index()
    {
        if(Gate::denies('coc-input')) {
            return redirect('/');
        }

    	$provinces = $this->province->getAll();

    	return view('input.ppcrv.index')
            ->with('provinces', $provinces)
    		->with('page_title', 'COC');
    }

    public function edit($provinceId)
    {
        if(Gate::denies('coc-input')) {
            return redirect('/');
        }

        $province = $this->province->get($provinceId);
        $presidents = $this->candidate->getAllByPosition('president');
        $vicePresidents = $this->candidate->getAllByPosition('vice_president');
        $senators = $this->candidate->getAllByPosition('senator');

        return view('input.ppcrv.edit')
            ->with('province', $province)
            ->with('presidents', $presidents)
            ->with('vicePresidents', $vicePresidents)
            ->with('senators', $senators)
            ->with('page_title', 'Edit COC');
    }

    public function update($provinceId, Request $request)
    {
        if(Gate::denies('coc-input')) {
            return redirect('/');
        }
        
        $candidates = $request->candidates;
        $province = $this->province->get($provinceId);
        
        foreach($candidates as $key => $candidate) {
            $data['count'] = $candidate;
            $this->ppcrvCount->update($key, $provinceId, $data);
        }

        $request->session()->flash('success_message', 'Successfully updated PPCRV COC, <strong>'.$province->name.'</strong>.');

        return redirect()->route('ppcrv-coc');
    }
}
