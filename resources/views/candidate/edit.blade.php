@extends('admin_template')

@section('content')
	{!! Form::model($candidate, ['method' => 'PUT', 'route' => ['admin.candidate.update', $candidate->id], 'files' => true]) !!}
	    @include('candidate.form', ['mode' => 'edit'])
	    {!! Form::submit('', ['id' => 'submit-form', 'class' => 'hidden']) !!}
	{!! Form::close() !!}
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
		    		<label for="submit-form" class="btn btn-primary btn-flat">Update Candidate</label>
		            <a href="{{ route('admin.candidate.index') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    	<div class="pull-right">
		    		{!! Form::open(['method' => 'DELETE', 'route' => ['admin.candidate.destroy', $candidate->id]]) !!}
						@if(is_null($candidate->deleted_at))
							<button type="submit" class="btn btn-danger btn-sm btn-flat" onclick="return confirm('Are you sure you want to delete candidate?')"><strong>Delete Candidate</strong></button>
						@endif
					{!! Form::close() !!}
		    	</div>
		    </div>
		</div>
	{!! Form::close() !!}
@endsection