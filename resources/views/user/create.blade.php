@extends('admin_template')

@section('head')
	
@endsection

@section('content')
	{!! Form::model($user, ['method' => 'POST', 'route' => ['admin.user.store'], 'files' => true]) !!}
	    @include('user.form', ['mode' => 'create'])
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
	    			<button type="submit" class="btn btn-primary btn-flat">Create User</button>
		            <a href="{{ route('admin.user.index') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    </div>
		</div>
	{!! Form::close() !!}	    
@endsection

@section('script')
	
@endsection