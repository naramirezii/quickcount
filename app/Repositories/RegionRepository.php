<?php

namespace App\Repositories;

use App\Region;
use Request;

class RegionRepository 
{
    /**
     * Get all the active clients
     * 
     * @return App\Models\Region Collection;
     */
    public function getAll()
    {
        return Region::all();
    }

    /**
     * Get all the clients including deactivated
     * 
     * @return App\Models\Region Collection;
     */
    public function getAllWithTrashed()
    {
        return Region::withTrashed()->get();
    }

    public function lists()
    {
        return Region::lists('name', 'id');
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\Region;
     */
    public function get($id) 
    {
        $region = Region::withTrashed()->find($id);

        return $region; 
    }
}