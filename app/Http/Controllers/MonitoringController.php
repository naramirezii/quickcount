<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProvinceRepository;
use App\Repositories\MonitoringRepository;
use App\Repositories\OfficialCountRepository;

use App\Http\Requests;
use Gate;

class MonitoringController extends Controller
{
	protected $province;

    function __construct(
    	ProvinceRepository $province,
    	MonitoringRepository $monitoring,
        OfficialCountRepository $officialCount)
    {
    	ini_set('xdebug.max_nesting_level', 200);

    	$this->province = $province;
    	$this->monitoring = $monitoring;
        $this->officialCount = $officialCount;

    	$this->middleware('auth');
    }

    public function index()
    {
        if(Gate::denies('monitoring')) {
            return redirect('/');
        }

    	$provinces = $this->province->getAll();
        $remittedProvinces = $this->officialCount->getRemittedProvinces();

        // Batch tagging of text
        $this->monitoring->batchTaggingOfText($remittedProvinces);

    	return view('input.manual.monitoring_index')
            ->with('provinces', $provinces)
            ->with('page_title', 'Monitoring');
    }

    public function edit($provinceId)
    {
        if(Gate::denies('monitoring')) {
            return redirect('/');
        }

    	$province = $this->province->get($provinceId);
    	$monitoring = $this->monitoring->getByProvince($provinceId);

    	return view('input.manual.monitoring_edit')
    		->with('monitoring', $monitoring)
    		->with('province', $province)
    		->with('page_title','Edit Monitoring');
    }

    public function update(Request $request, $provinceId)
    {
        if(Gate::denies('monitoring')) {
            return redirect('/');
        }
        
    	$province = $this->province->get($provinceId);

    	$monitoring = $this->monitoring->update($provinceId, $request->all());

    	$request->session()->flash('success_message', 'Successfully updated monitoring for <strong>'.$province->name.'</strong>.');

        return redirect()->route('manual.monitoring');
    }
}
