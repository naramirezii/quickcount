<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\CandidateRepository;
use App\Repositories\RegionRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\OfficialCountRepository;
use App\Candidate;

class RankingController extends Controller
{
    protected $candidate;
    protected $region;
    protected $province;
    protected $officialCount;

	function __construct(
        CandidateRepository $candidate,
        RegionRepository $region,
        ProvinceRepository $province,
        OfficialCountRepository $officialCount)
	{
        ini_set('xdebug.max_nesting_level', 200);

        $this->candidate = $candidate;
        $this->region = $region;
        $this->province = $province;
        $this->officialCount = $officialCount;

		// $this->middleware('auth');
	}

    public function overall()
    {
        $candidates = $this->officialCount->getOverallRankingByPosition('president');
        $overallVoteCount = $this->officialCount->getOverallVoteCount('president');

    	return view('ranking.overall')
            ->with('candidates', $candidates)
            ->with('overallVoteCount', $overallVoteCount)
            ->with('page_title', 'Overall Ranking - President');
    }

    public function overallVicePresident()
    {
        $candidates = $this->officialCount->getOverallRankingByPosition('vice_president');
        $overallVoteCount = $this->officialCount->getOverallVoteCount('vice_president');

        return view('ranking.overall_vice_president')
            ->with('candidates', $candidates)
            ->with('overallVoteCount', $overallVoteCount)
            ->with('page_title', 'Overall Ranking - Vice President');
    }

    public function regional()
    {
        $regions = $this->region->getAll();
        $candidates = $this->candidate->getAllByPosition('president');

        return view('ranking.regional')
            ->with('regions', $regions)
            ->with('candidates', $candidates)
            ->with('page_title', 'Regional Ranking - President');
    }

    public function regionalVicePresident()
    {
        $regions = $this->region->getAll();
        $candidates = $this->candidate->getAllByPosition('vice_president');

        return view('ranking.regional_vice_president')
            ->with('regions', $regions)
            ->with('candidates', $candidates)
            ->with('page_title', 'Regional Ranking - Vice President');
    }

    public function provincial()
    {
        $provinces = $this->province->getAll();
        $candidates = $this->candidate->getAllByPosition('president');

        return view('ranking.provincial')
            ->with('provinces', $provinces)
            ->with('candidates', $candidates)
            ->with('page_title', 'Provincial - President');
    }

    public function provincialVicePresident()
    {
        $provinces = $this->province->getAll();
        $candidates = $this->candidate->getAllByPosition('vice_president');

        return view('ranking.provincial_vice_president')
            ->with('provinces', $provinces)
            ->with('candidates', $candidates)
            ->with('page_title', 'Provincial - Vice President');
    }

    public function cocPresident()
    {
        $provinces = $this->province->getAll();
        $candidates = $this->candidate->getAllByPosition('president');

        return view('ranking.coc_president')
            ->with('provinces', $provinces)
            ->with('candidates', $candidates)
            ->with('page_title', 'COC - President');
    }

    public function cocVicePresident()
    {
        $provinces = $this->province->getAll();
        $candidates = $this->candidate->getAllByPosition('vice_president');

        return view('ranking.coc_vice_president')
            ->with('provinces', $provinces)
            ->with('candidates', $candidates)
            ->with('page_title', 'COC - Vice President');
    }

    public function countReceivedPresident()
    {
        $totalProvinces = sizeof($this->province->getAll());
        $remittedProvincesCount = sizeof($this->officialCount->getRemittedProvinces());
        $remittanceRate = $this->officialCount->getRemittanceRate();

        return view('ranking.maps.count_received_president')
            ->with('totalProvinces', $totalProvinces)
            ->with('remittedProvincesCount', $remittedProvincesCount)
            ->with('remittanceRate', $remittanceRate)
            ->with('page_title', 'Vote Count Received Per Area (President)');
    }

    public function countReceivedVicePresident()   
    {
        $totalProvinces = sizeof($this->province->getAll());
        $remittedProvincesCount = sizeof($this->officialCount->getRemittedProvinces());
        $remittanceRate = $this->officialCount->getRemittanceRate();

        return view('ranking.maps.count_received_vice_president')
            ->with('totalProvinces', $totalProvinces)
            ->with('remittedProvincesCount', $remittedProvincesCount)
            ->with('remittanceRate', $remittanceRate)
            ->with('page_title', 'Vote Count Received Per Area (Vice President)');
    }

    public function perAreaPresident()   
    {
        return view('ranking.maps.per_area_president')
            ->with('page_title', 'President by Area');
    }

    public function perAreaVicePresident()   
    {
        return view('ranking.maps.per_area_vice_president')
            ->with('page_title', 'Vice President by Area');
    }
}
