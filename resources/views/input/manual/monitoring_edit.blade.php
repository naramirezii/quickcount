@extends('admin_template')

@section('content')
	{!! Form::model($monitoring, ['method' => 'POST', 'route' => ['manual.monitoring.update', $province->id]]) !!}
	    <div class="row">
		  <div class="col-xs-12">
		    <div class="box">
		    	<div class="box-header with-border">
		        <!-- <h3 class="box-title">Enroller Profile</h3> -->
		      </div><!-- /.box-header -->
		      <div class="box-body">
		        <div class="form-group  {{ $errors->has('province') ? 'has-error' : '' }}">
		          <label>Province</label>
		          {{ $province->name }} ({{ $province->code }})
		        </div>
		        <div class="form-group  {{ $errors->has('monitoring') ? 'has-error' : '' }}">
		          <label>Monitoring</label><br>
		          {!! Form::checkbox('text', '1') !!} Text <br>
		          {!! Form::checkbox('viber', '1') !!} Viber <br>
		          {!! Form::checkbox('call', '1') !!} Call <br>
		        </div>
		      </div><!-- /.box-body -->
		    </div><!-- /.box -->
		  </div><!-- /.col -->
		</div><!-- /.row -->
	    {!! Form::submit('', ['id' => 'submit-form', 'class' => 'hidden']) !!}
	{!! Form::close() !!}
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
		    		<label for="submit-form" class="btn btn-primary btn-flat">Update</label>
		            <a href="{{ route('manual.monitoring') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    </div>
		</div>
@endsection

@section('script')
@endsection