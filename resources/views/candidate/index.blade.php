@extends('admin_template')

@section('head')
	<!-- DATA TABLES -->
    <link href="{{ asset('admin_theme/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		<a href="{{ route('admin.candidate.create') }}" class="btn btn-primary btn-flat fa fa-plus"> Add New</a>
		<br/><br/>

		<div class="box">
	      	<div class="box-header">
	      		<!-- <h3 class="box-title">President</h3> -->
	      	</div><!-- /.box-header -->
	      	<div class="box-body">
		      	<table id="data_table" class="table table-bordered table-striped">
		      		<thead>
		      			<tr>
		      				<th>First Name</th>
		      				<th>Last Name</th>
		      				<th>Middle Name</th>
		      				<th>Code</th>
		      				<th>Position</th>
		      				<th></th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      			@foreach($candidates as $candidate)
			      			<tr>
			      				<td>{{ $candidate->first_name }}</td>
			      				<td>{{ $candidate->last_name }}</td>
			      				<td>{{ $candidate->middle_name }}</td>
			      				<td>{{ $candidate->code }}</td>
			      				<td>{{ $candidate->position }}</td>
			      				<td align="center"><a href="{{ route('admin.candidate.edit', $candidate->id) }}"><i class="fa fa-pencil"></i></a></td>
			      			</tr>
		      			@endforeach
		      		</tbody>
		      	</table>
		    </div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
	<!-- DATA TABLES SCRIPT -->
	<script src="{{ asset("admin_theme/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
	<script src="{{ asset("admin_theme/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>

	<script type="text/javascript">
		$(function () {
			var table = $('#data_table').DataTable({
				"paging": false,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"autoWidth": true,
				"scrollX": true
			});
		});		
	</script>
@endsection