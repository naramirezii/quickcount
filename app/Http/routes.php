<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return redirect()->action('RankingController@overall');
    });

    // Auth
    Route::group(['namespace' => 'Auth'], function(){
        Route::get('login', [
            'as'   => 'auth.login',
            'uses' =>  'AuthController@getLogin'
        ]);
        
        Route::post('login',[
            'as'   => 'auth.login',
            'uses' =>  'AuthController@postLogin'
        ]);

        Route::get('logout', [
            'as'   => 'auth.logout',
            'uses' =>  'AuthController@logout'
        ]);
    });

    // Ranking
    Route::get('ranking/overall', [
        'as' => 'ranking.overall',
        'uses' => 'RankingController@overall'
    ]);
    Route::get('ranking/overall/vice-president', [
        'as' => 'ranking.overall-vice-president',
        'uses' => 'RankingController@overallVicePresident'
    ]);
    Route::get('ranking/regional', [
        'as' => 'ranking.regional',
        'uses' => 'RankingController@regional'
    ]);
    Route::get('ranking/regional/vice-president', [
        'as' => 'ranking.regional-vice-president',
        'uses' => 'RankingController@regionalVicePresident'
    ]);
    Route::get('ranking/provincial', [
        'as' => 'ranking.provincial',
        'uses' => 'RankingController@provincial'
    ]);
    Route::get('ranking/provincial/vice-president', [
        'as' => 'ranking.provincial-vice-president',
        'uses' => 'RankingController@provincialVicePresident'
    ]);
    Route::get('ranking/coc/president', [
        'as' => 'ranking.coc-president',
        'uses' => 'RankingController@cocPresident'
    ]);
    Route::get('ranking/coc/vice-president', [
        'as' => 'ranking.coc-vice-president',
        'uses' => 'RankingController@cocVicePresident'
    ]);

    // Maps
    Route::get('ranking/map/count-received-president', [
        'as' => 'ranking.count-received-president',
        'uses' => 'RankingController@countReceivedPresident'
    ]);
    Route::get('ranking/map/count-received-vice-president', [
        'as' => 'ranking.count-received-vice-president',
        'uses' => 'RankingController@countReceivedVicePresident'
    ]);
    Route::get('ranking/map/per-area-president', [
        'as' => 'ranking.per-area-president',
        'uses' => 'RankingController@perAreaPresident'
    ]);
    Route::get('ranking/map/per-area-vice-president', [
        'as' => 'ranking.per-area-vice-president',
        'uses' => 'RankingController@perAreaVicePresident'
    ]);
    

    // Reports
    Route::get('report/voters-turnout/regional', [
        'as' => 'report.voters-turnout-regional',
        'uses' => 'ReportController@votersTurnoutRegional'
    ]);
    Route::get('report/voters-turnout/provincial', [
        'as' => 'report.voters-turnout-provincial',
        'uses' => 'ReportController@votersTurnoutProvincial'
    ]);
    Route::get('report/discrepancies-president', [
        'as' => 'report.discrepancies-president',
        'uses' => 'ReportController@discrepanciesPresident'
    ]);
    Route::get('report/discrepancies-vice-president', [
        'as' => 'report.discrepancies-vice-president',
        'uses' => 'ReportController@discrepanciesVicePresident'
    ]);
    Route::get('report/registered-vs-actual-president', [
        'as' => 'report.registered-vs-actual-president',
        'uses' => 'ReportController@registeredVsActualPresident'
    ]);
    Route::get('report/registered-vs-actual-vice-president', [
        'as' => 'report.registered-vs-actual-vice-president',
        'uses' => 'ReportController@registeredVsActualVicePresident'
    ]);
    Route::get('report/registered-vs-coc-president', [
        'as' => 'report.registered-vs-coc-president',
        'uses' => 'ReportController@registeredVsCocPresident'
    ]);
    Route::get('report/registered-vs-coc-vice-president', [
        'as' => 'report.registered-vs-coc-vice-president',
        'uses' => 'ReportController@registeredVsCocVicePresident'
    ]);

    // Input
    // Admin
    Route::group(['prefix' => 'input'], function(){
        Route::get('import', [
            'as' => 'import',
            'uses' => 'ImportController@import'
        ]);

        // Manual Quick Count
        Route::get('manual', [
            'as' => 'manual',
            'uses' => 'OfficialCountController@presidentIndex'
        ]);
        Route::get('manual/{candidate_id}/{province_id}/edit', [
            'as' => 'manual.edit',
            'uses' => 'OfficialCountController@editCount'
        ]);
        Route::post('manual/{candidate_id}/{province_id}/update', [
            'as' => 'manual.update',
            'uses' => 'OfficialCountController@updateCount'
        ]);
        Route::get('manual/vice-president', [
            'as' => 'manual.vice-president',
            'uses' => 'OfficialCountController@vicePresidentIndex'
        ]);
        Route::get('manual/vice-president/{candidate_id}/{province_id}/edit', [
            'as' => 'manual.vice-president.edit',
            'uses' => 'OfficialCountController@editVicePresidentCount'
        ]);
        Route::post('manual/vice-president/{candidate_id}/{province_id}/update', [
            'as' => 'manual.vice-president.update',
            'uses' => 'OfficialCountController@updateVicePresidentCount'
        ]);
        // Monitoring
        Route::get('monitoring', [
            'as' => 'manual.monitoring',
            'uses' => 'MonitoringController@index'
        ]);
        Route::get('monitoring/{province_id}/edit', [
            'as' => 'manual.monitoring.edit',
            'uses' => 'MonitoringController@edit'
        ]);
        Route::post('monitoring/{province_id}/update', [
            'as' => 'manual.monitoring.update',
            'uses' => 'MonitoringController@update'
        ]);

        // PPCRV COC
        Route::get('ppcrv-coc', [
            'as' => 'ppcrv-coc',
            'uses' => 'PpcrvCountController@index'
        ]);
        Route::get('ppcrv-coc/{province_id}/edit', [
            'as' => 'ppcrv-coc.edit',
            'uses' => 'PpcrvCountController@edit'
        ]);
        Route::post('ppcrv-coc/{province_id}/update', [
            'as' => 'ppcrv-coc.update',
            'uses' => 'PpcrvCountController@update'
        ]);
    });

    // Admin
    Route::group(['prefix' => 'admin'], function(){
        Route::resource('candidate', 'CandidateController');
        // User
        Route::get('user/{id}/change-password', [
            'as' => 'admin.user.change-password',
            'uses' =>  'UserController@changePassword'
        ]);
        Route::post('user/{id}/updatePassword', [
            'as' => 'admin.user.update-password',
            'uses' => 'UserController@updatePassword'
        ]);
        Route::resource('user', 'UserController');

        // Configuration
        Route::get('config/reset-database', [
            'as' => 'config.reset-database',
            'uses' => 'ConfigController@resetDatabase'
        ]);
        Route::post('config/save-reset-database', [
            'as' => 'config.save-reset-database',
            'uses' => 'ConfigController@saveResetDatabase'
        ]);
    });
    

});
