@extends('admin_template')

@section('head')
@endsection

@section('breadcrumbs')
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 text-center">
			<h3>{{ $province->name }}</h3>
			<p>Zipcode: {{ $province->zip_code }}</p>
		</div>
	</div>
	{!! Form::open(['method' => 'POST', 'route' => ['ppcrv-coc.update', $province->id]]) !!}
	    <div class="row">
		  <div class="col-xs-12">
		    <div class="box">
		    	<div class="box-header with-border">
		        <h3 class="box-title">President</h3>
		      </div><!-- /.box-header -->
		      <div class="box-body">
		      @foreach($presidents as $president)
		        <div class="form-group col-lg-4 {{ $errors->has('count') ? 'has-error' : '' }}">
		          <label>{{ $president->name }}</label>
		          {!! Form::text("candidates[$president->id]", $province->getCandidatePpcrvCount($president->id, $province->id), ['class' => 'form-control']) !!}
		        </div>
		      @endforeach
		      </div><!-- /.box-body -->
		    </div><!-- /.box -->
		  </div><!-- /.col -->
		</div><!-- /.row -->

		<div class="row">
		  <div class="col-xs-12">
		    <div class="box">
		    	<div class="box-header with-border">
		        <h3 class="box-title">Vice President</h3>
		      </div><!-- /.box-header -->
		      <div class="box-body">
		      @foreach($vicePresidents as $vicePresident)
		        <div class="form-group col-lg-4 {{ $errors->has('count') ? 'has-error' : '' }}">
		          <label>{{ $vicePresident->name }}</label>
		          {!! Form::text("candidates[$vicePresident->id]", $province->getCandidatePpcrvCount($vicePresident->id, $province->id), ['class' => 'form-control']) !!}
		        </div>
		      @endforeach
		      </div><!-- /.box-body -->
		    </div><!-- /.box -->
		  </div><!-- /.col -->
		</div><!-- /.row -->

		<div class="row">
		  <div class="col-xs-12">
		    <div class="box">
		    	<div class="box-header with-border">
		        <h3 class="box-title">Senator</h3>
		      </div><!-- /.box-header -->
		      <div class="box-body">
		      @foreach($senators as $senator)
		        <div class="form-group col-lg-4 {{ $errors->has('count') ? 'has-error' : '' }}">
		          <label>{{ $senator->name }}</label>
		          {!! Form::text("candidates[$senator->id]", $province->getCandidatePpcrvCount($senator->id, $province->id), ['class' => 'form-control']) !!}
		        </div>
		      @endforeach
		      </div><!-- /.box-body -->
		    </div><!-- /.box -->
		  </div><!-- /.col -->
		</div><!-- /.row -->


	    {!! Form::submit('', ['id' => 'submit-form', 'class' => 'hidden']) !!}
	
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
		    		<label for="submit-form" class="btn btn-primary btn-flat">Update Count</label>
		            <a href="{{ route('manual.vice-president') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    </div>
		</div>
	{!! Form::close() !!}
@endsection

@section('script')
@endsection