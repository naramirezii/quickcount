<?php

namespace App\Repositories;

use App\Province;
use Request;

class ProvinceRepository 
{
    /**
     * Get all the active clients
     * 
     * @return App\Models\Province Collection;
     */
    public function getAll()
    {
        return Province::all();
    }

    /**
     * Get all the clients including deactivated
     * 
     * @return App\Models\Province Collection;
     */
    public function getAllWithTrashed()
    {
        return Province::withTrashed()->get();
    }

    public function lists()
    {
        return Province::lists('name', 'id');
    }

    /**
     * Get single instance
     * @param  $id
     * @return App\Models\Province;
     */
    public function get($id) 
    {
        $province = Province::find($id);

        return $province; 
    }
}