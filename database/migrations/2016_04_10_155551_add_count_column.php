<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('official_counts', function(Blueprint $table)
        {
            $table->integer('count')->after('province_id')->nullable();
        });

        Schema::table('ppcrv_counts', function(Blueprint $table)
        {
            $table->integer('count')->after('province_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
