<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitoring extends Model
{
    protected $fillable = ['province_id', 'text', 'viber', 'call'];

    public function province()
    {
    	return $this->belongsTo(Province::class);
    }
}
