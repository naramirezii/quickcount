@extends('admin_template')

@section('head')
@endsection

@section('breadcrumbs')
@endsection

@section('content')
	{!! Form::model($officialCount, ['method' => 'POST', 'route' => ['manual.vice-president.update', $candidate->id, $province->id], 'files' => true]) !!}
	    <div class="row">
		  <div class="col-xs-12">
		    <div class="box">
		    	<div class="box-header with-border">
		        <!-- <h3 class="box-title">Enroller Profile</h3> -->
		      </div><!-- /.box-header -->
		      <div class="box-body">
		      	<div class="form-group  {{ $errors->has('candidate') ? 'has-error' : '' }}">
		          <label>Candidate</label>
		          {{ $candidate->first_name . ' ' . $candidate->last_name }}
		        </div>
		        <div class="form-group  {{ $errors->has('province') ? 'has-error' : '' }}">
		          <label>Province</label>
		          {{ $province->name }}
		        </div>
		        <div class="form-group  {{ $errors->has('count') ? 'has-error' : '' }}">
		          <label>Count</label>
		          {!! Form::text('count', null, ['class' => 'form-control']) !!}
		        </div>
		      </div><!-- /.box-body -->
		    </div><!-- /.box -->
		  </div><!-- /.col -->
		</div><!-- /.row -->
	    {!! Form::submit('', ['id' => 'submit-form', 'class' => 'hidden']) !!}
	{!! Form::close() !!}
		<div class="row">
		    <div class="col-xs-12">
		    	<div class="pull-left">
		    		<label for="submit-form" class="btn btn-primary btn-flat">Update Count</label>
		            <a href="{{ route('manual.vice-president') }}" class="btn"><u>Cancel</u></a>
		    	</div>
		    </div>
		</div>
	{!! Form::close() !!}
@endsection

@section('script')
@endsection