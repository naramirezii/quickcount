@extends('admin_template')

@section('head')
	<!-- DATA TABLES -->
    <link href="{{ asset('admin_theme/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 text-center">
		<a href="{{ route('report.registered-vs-coc-president') }}" class="btn btn-default btn-flat btn-sm">President</a>
		<a href="{{ route('report.registered-vs-coc-vice-president') }}" class="btn btn-primary btn-flat btn-sm">Vice President</a>
		<br><br>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div class="box">
	      	<div class="box-header">
	      		<!-- <h3 class="box-title">President</h3> -->
	      	</div><!-- /.box-header -->
	      	<div class="box-body">
		      	<table id="data_table" class="table table-bordered table-striped">
		      		<thead>
		      			<tr>
		      				<th>Province</th>
		      				<th>Region</th>
		      				<th>Registered Voters</th>
		      				<th>Actual</th>
		      				<th>Difference</th>
		      			</tr>
		      		</thead>
		      		<tbody>
		      		@foreach($provinces as $province)
		      			<tr>
		      				<td>{{ $province->name }} ({{ $province->code }})</td>
		      				<td>{{ $province->region->name }}</td>
		      				<td align="right">{{ number_format($province->registered_voters) }}</td>
		      				<td align="right">
		      					@if($province->getCocVotersTurnout('vice_president', $province->id))
		      						{{ number_format($province->getCocVotersTurnout('vice_president', $province->id)) }}
		      					@endif
		      				</td>
		      				<td align="right" 
		      					@if($province->getCocVotersTurnout('vice_president', $province->id) > $province->registered_voters)
		      						class="danger"
		      					@endif
		      					>
		      					@if($province->getCocVotersTurnout('vice_president', $province->id))
		      						{{ number_format($province->getRegisteredVsCocCount('vice_president', $province->id)) }}
		      					@endif
		      				</td>
		      			</tr>
		      		@endforeach
		      		</tbody>
		      	</table>
		    </div><!-- /.box-body -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('script')
	<!-- DATA TABLES SCRIPT -->
	<script src="{{ asset("admin_theme/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>
	<script src="{{ asset("admin_theme/plugins/datatables/dataTables.bootstrap.js") }}" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js" type="text/javascript"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.0/js/buttons.flash.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(function () {
			var table = $('#data_table').DataTable({
				"paging": false,
				"lengthChange": false,
				"searching": true,
				"ordering": true,
				"info": false,
				"autoWidth": true,
				"scrollX": true,
				dom: 'Bfrtip',
				buttons: [
		            'excel', 'print'
		        ]
			});
		});		
	</script>
@endsection