<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Region extends Model
{
    public function provinces()
    {
        return $this->hasMany(Province::class);
    }

    public function getCandidateQuickCount($candidateId, $regionId)
    {
        $count = 0;

        foreach($this->provinces as $province) {
            $quickCount = OfficialCount::where('candidate_id', $candidateId)
                ->where('province_id', $province->id)
                ->orderBy('id', 'desc')
                ->first();

            if($quickCount) $count += $quickCount->count;
        }

    	if($count)
    		return $count;
    	else
    		return null;
    }

    public function getCandidatePercentage($candidateId, $regionId)
    {
        $candidate = Candidate::find($candidateId);

        if($this->getVotersTurnout($candidate->position, $regionId)) {
            $result = $this->getCandidateQuickCount($candidateId, $regionId) / $this->getVotersTurnout($candidate->position, $regionId) * 100;
            return number_format($result, 2, '.', ',');
        }

    }

    public function getRegisteredVoters($regionId)
    {
        $registeredVoters = Province::select(DB::raw('SUM(registered_voters) as count'))
            ->where('region_id', $regionId)
            ->join('regions', 'regions.id', '=', 'provinces.region_id')
            ->first();

        if($registeredVoters)
            return $registeredVoters->count;
        else
            return null;
    }

    public function getVotersTurnout($position, $regionId)
    {
        $count = 0;
        $candidates = Candidate::where('position', $position)->get();

        foreach($candidates as $candidate) {
            $count += $this->getCandidateQuickCount($candidate->id, $regionId);
        }

        return $count;
    }

    public function getVotersTurnoutPercentage($regionId)
    {
        if($this->getVotersTurnout('president', $regionId))
            return $this->getVotersTurnout('president', $regionId) / $this->getRegisteredVoters($regionId) * 100;
        else
            return null;
    }
}
