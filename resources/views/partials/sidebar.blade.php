<aside class="main-sidebar">
  <section class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header" style="color:#fff;">MENU</li>
      <li class="treeview {{ (Request::segment(1) == 'ranking') ? 'active' : '' }}">
        <a href="#"><i class="fa fa-bar-chart"></i> <span>Ranking</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li @if(Request::segment(2) == 'overall') class="active" @endif><a href="{{ url('ranking/overall') }}">Overall</a></li>
          <li @if(Request::segment(2) == 'regional') class="active" @endif><a href="{{ url('ranking/regional') }}">Regional</a></li>
          <li @if(Request::segment(2) == 'provincial') class="active" @endif><a href="{{ url('ranking/provincial') }}">Provincial</a></li>
          <li @if(Request::segment(2) == 'coc') class="active" @endif><a href="{{ url('ranking/coc/president') }}">COC</a></li>
          <li @if(Request::segment(2) == 'map') class="active" @endif><a href="{{ url('ranking/map/count-received-president') }}">Map</a></li>
        </ul>
      </li>

      @if(auth()->user())

        @can('view-reports')
          <li class="treeview {{ (Request::segment(1) == 'report') ? 'active' : '' }}">
            <a href="#"><i class="fa fa-file-text"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li @if(Request::segment(2) == 'voters-turnout') class="active" @endif><a href="{{ url('report/voters-turnout/regional') }}">Voters Turnout</a></li>
              <li @if(Request::segment(2) == 'discrepancies-president') class="active" @endif><a href="{{ url('report/discrepancies-president') }}">Discrepancies</a></li>
              <li @if(Request::segment(2) == 'registered-vs-actual-president') class="active" @endif><a href="{{ url('report/registered-vs-actual-president') }}">Registered vs Actual</a></li>
              <li @if(Request::segment(2) == 'registered-vs-coc-president') class="active" @endif><a href="{{ url('report/registered-vs-coc-president') }}">Registered vs Actual (COC)</a></li>
            </ul>
          </li>
        @endcan

          <li class="treeview {{ (Request::segment(1) == 'setting') ? 'active' : '' }}">
            <a href="#"><i class="fa fa-keyboard-o"></i> <span>Votes Input</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
            @can('import')
              <li @if(Request::segment(2) == 'import') class="active" @endif><a href="{{ url('input/import') }}">Import</a></li>
            @endcan
            @can('manual-input')
              <li @if(Request::segment(2) == 'manual') class="active" @endif><a href="{{ url('input/manual') }}">Manual Input</a></li>
            @endcan
            @can('coc-input')
              <li @if(Request::segment(2) == 'ppcrv-coc') class="active" @endif><a href="{{ url('input/ppcrv-coc') }}">COC</a></li>
            @endcan
            @can('monitoring')
              <li @if(Request::segment(2) == 'monitoring') class="active" @endif><a href="{{ url('input/monitoring') }}">Monitoring</a></li>
            @endcan
            </ul>
          </li>

        @can('manage-admin')
          <li class="treeview {{ (Request::segment(1) == 'admin') ? 'active' : '' }}">
            <a href="#"><i class="fa fa-gear"></i> <span>Admin</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li @if(Request::segment(2) == 'candidate') class="active" @endif><a href="{{ url('admin/candidate') }}">Candidates</a></li>
              <li @if(Request::segment(2) == 'user') class="active" @endif><a href="{{ url('admin/user') }}">Users</a></li>
              <li @if(Request::segment(3) == 'reset-database') class="active" @endif><a href="{{ url('admin/config/reset-database') }}">Reset Database</a></li>
            </ul>
          </li>
        @endcan

      @endif
      
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>