<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CandidateRepository;
use App\Http\Requests;
use App\Http\Requests\CandidateRequest;
use Gate;

class CandidateController extends Controller
{
    protected $candidate;

    function __construct(CandidateRepository $candidate)
    {
        ini_set('xdebug.max_nesting_level', 200);

        $this->candidate = $candidate;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }

        $candidates = $this->candidate->getAll();

        return view('candidate.index')
            ->with('candidates', $candidates)
            ->with('page_title', 'List of Candidates');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }

        $positions = $this->getPositions();

        return view('candidate.create')
            ->with('candidate', null)
            ->with('positions', $positions)
            ->with('page_title', 'Create Candidate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CandidateRequest $request)
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }

        $candidate = $this->candidate->create($request->all());

        $request->session()->flash('success_message', 'Successfully added candidate, <strong>'.$candidate->last_name.'</strong>.');

        return redirect()->route('admin.candidate.index', $candidate->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }

        $candidate = $this->candidate->get($id);
        $positions = $this->getPositions();

        return view('candidate/edit')
            ->with('candidate', $candidate)
            ->with('positions', $positions)
            ->with('page_title', 'Edit Candidate');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CandidateRequest $request, $id)
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }

        $candidate = $this->candidate->update($id, $request->all());

        $request->session()->flash('success_message', 'Successfully updated candidate, <strong>'.$candidate->last_name.'</strong>.');

        return redirect()->route('admin.candidate.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }

        $candidate = $this->candidate->destroy($id);
        
        $request->session()->flash('success_message', 'Successfully deleted candidate, <strong>'.$candidate->last_name.'</strong>.');
        
        return redirect()->route('admin.candidate.index');
    }

    private function getPositions()
    {
        if(Gate::denies('manage-candidates')) {
            return redirect('/');
        }
        
        return array('president' => 'President',
            'vice_president' => 'Vice President',
            'senator' => 'Senator');
    }
}
